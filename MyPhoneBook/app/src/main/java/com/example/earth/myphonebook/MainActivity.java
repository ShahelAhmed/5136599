package com.example.earth.myphonebook;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import android.app.Activity;

import android.text.Editable;
import android.text.TextWatcher;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {
    //variables for contact such as name and phone
    EditText nameText, phoneText;
    List<Contact> theContacts = new ArrayList<Contact>();
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //finds id which will be used
        nameText = (EditText) findViewById(R.id.txtName);
        phoneText = (EditText) findViewById(R.id.txtPhone);
        listView=(ListView)findViewById(R.id.listView);
        //tabs
        TabHost hostTab = (TabHost) findViewById(R.id.tabHost);
        hostTab.setup();

        TabHost.TabSpec tabSpec = hostTab.newTabSpec("New Contact");
        tabSpec.setContent(R.id.NewContact);
        tabSpec.setIndicator("New Contact");
        hostTab.addTab(tabSpec);

        tabSpec = hostTab.newTabSpec("All Contacts");
        tabSpec.setContent(R.id.allContacts);
        tabSpec.setIndicator("All Contacts");
        hostTab.addTab(tabSpec);

        //stores contact details
        final Button addButton = (Button) findViewById(R.id.buttonAdd);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addContacts(nameText.getText().toString(), phoneText.getText().toString());
                populateTheList();
                Toast.makeText(getApplicationContext(), nameText.getText().toString() + " has been added!", Toast.LENGTH_SHORT).show();

            }
        });
        //Adds a TextWatcher to the list of those whose methods are called whenever this TextView's text changes.
         nameText.addTextChangedListener(new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        addButton.setEnabled(!nameText.getText().toString().trim().isEmpty());
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    });
    }
    //populates the list
    private void populateTheList(){
        ArrayAdapter<Contact> theAdapter= new ContactListAdapter();
        listView.setAdapter(theAdapter);
    }

    private void addContacts(String name, String phone) {
        theContacts.add(new Contact(name, phone));
    }

    private void createContacts(String name, String phone){
        theContacts.add(new Contact(name, phone));
    }

    private class ContactListAdapter extends ArrayAdapter<Contact>{
        public ContactListAdapter(){
            super (MainActivity.this, R.layout.listview, theContacts);
        }
        //view contacts
        @Override
        public View getView(int position, View theView, ViewGroup parent){
            if(theView == null)
                theView=getLayoutInflater().inflate(R.layout.listview, parent, false);
            Contact recentContact = theContacts.get(position);

            TextView name= (TextView) theView.findViewById(R.id.textLName);
            name.setText(recentContact.getName());
            TextView phone = (TextView) theView.findViewById(R.id.textLPhone);
            phone.setText(recentContact.get_phone());

            return theView;
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
