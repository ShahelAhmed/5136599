package com.example.earth.myphonebook;

/**
 * Created by Earth on 03/05/2015.
 */
public class Contact {
    private String _name, _phone;

    public Contact(String name, String phone){
        _name = name;
        _phone= phone;

    }

    public String getName(){
        return _name;
    }

    public String get_phone(){
        return _phone;
    }
}
