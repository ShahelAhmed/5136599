package com.example.earth.stacktest1;

import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity {

    int total = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final LinearLayout linearLayout = (LinearLayout) findViewById(R.id.linear_layout);

        Button chicken = (Button) findViewById(R.id.buttonChicken);
        Button chips = (Button) findViewById(R.id.buttonChips);
        Button fish = (Button) findViewById(R.id.buttonFish);
        Button rice = (Button) findViewById(R.id.buttonRice);
        Button lamb = (Button) findViewById(R.id.buttonLamb);
        Button lobster = (Button) findViewById(R.id.buttonLobster);
        Button softdrink = (Button) findViewById(R.id.buttonSoft);
        Button beer = (Button) findViewById(R.id.buttonBeer);
        Button wine = (Button) findViewById(R.id.buttonWine);

        chips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                total+=10;

                TextView textView = new TextView(MainActivity.this);
                textView.setText("You have added chips which increases your total to £" + total);

                textView.setTextColor(Color.parseColor("#3399FF"));

                linearLayout.addView(textView);
            }
        });

        chicken.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                total+=15;

                TextView textView = new TextView(MainActivity.this);
                textView.setText("You have added Chicken which increases your total to £" + total);
                //sets text colour
                textView.setTextColor(Color.parseColor("#3399FF"));

                linearLayout.addView(textView);
            }
        });

        fish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                total+=11;

                TextView textView = new TextView(MainActivity.this);
                textView.setText("You have added Fish which increases your total to £" + total);
                textView.setTextColor(Color.parseColor("#3399FF"));

                linearLayout.addView(textView);
            }
        });

        rice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                total+=6;

                TextView textView = new TextView(MainActivity.this);
                textView.setText("You have added rice which increases your total to £" + total);
                textView.setTextColor(Color.parseColor("#3399FF"));

                linearLayout.addView(textView);
            }
        });
        lamb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                total+=13;

                TextView textView = new TextView(MainActivity.this);
                textView.setText("You have added lamb which increases your total to £" + total);
                textView.setTextColor(Color.parseColor("#3399FF"));

                linearLayout.addView(textView);
            }
        });
        lobster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                total+=20;

                TextView textView = new TextView(MainActivity.this);
                textView.setText("You have added lobster which increases your total to £" + total);
                textView.setTextColor(Color.parseColor("#3399FF"));

                linearLayout.addView(textView);
            }
        });
        softdrink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                total+=1;

                TextView textView = new TextView(MainActivity.this);
                textView.setText("You have added drink which increases your total to £" + total);
                textView.setTextColor(Color.parseColor("#3399FF"));

                linearLayout.addView(textView);
            }
        });
        beer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                total+=3;

                TextView textView = new TextView(MainActivity.this);
                textView.setText("You have added beer which increases your total to £" + total);
                textView.setTextColor(Color.parseColor("#3399FF"));

                linearLayout.addView(textView);
            }
        });
        wine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                total+=5;

                TextView textView = new TextView(MainActivity.this);
                textView.setText("You have added Fish which increases your total to £" + total);
                textView.setTextColor(Color.parseColor("#3399FF"));

                linearLayout.addView(textView);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
